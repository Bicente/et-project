from django.urls import path, include
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('',views.index,name="index"),
    path('Registro/',views.registrar,name="registrar"),
    path('Registro/Crear/',views.crear,name ="crear"),
    path('CrearTienda/',views.crearTienda,name ="crearTienda"),
    path('CrearProducto/',views.crearProducto,name ="crearProducto"),
    path('login_iniciar/',views.login_iniciar,name="login_iniciar"),
    path('cerrar_sesion/',views.cerrar_sesion,name="cerrar_sesion"),
    url(r'^oauth/', include('social_django.urls', namespace='social')),
]