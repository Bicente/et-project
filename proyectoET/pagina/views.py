from django.shortcuts import render
from .models import Persona,Producto,Tienda
from django.shortcuts import redirect
#importar user
from django.contrib.auth.models import User, Group
#sistema de autenticación 
from django.contrib.auth import authenticate,logout, login as auth_login
#serializers
from .models import Persona
from .models import Tienda
from .models import Producto
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from rest_framework import viewsets, serializers
from proyectoET.serializers import PersonaSerializer, TiendaSerializer, ProductoSerializer
# Create your views here.

def index(request):
    count = Persona.objects.all().count()
    return render(request,'index.html',{'tienda':Tienda.objects.all(),'producto':Producto.objects.all()})

def registrar(request):
    return render(request,'registrar.html',{})

def crear(request):
    username = request.POST.get('username','')
    password = request.POST.get('password','')
    nombre = request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    rut = request.POST.get('rut','')
    fechNacimiento = request.POST.get('fechNacimiento','')
    correo = request.POST.get('correo','')
    region = request.POST.get('region','')
    comuna = request.POST.get('comuna','')
    persona = Persona(username=username,password=password,nombre=nombre,apellido=apellido,rut=rut,fechNacimiento=fechNacimiento,correo=correo,region=region,comuna=comuna)
    persona.save()

    persona = User.objects.create_user(username, correo, password)
    persona.save()

    return redirect('index')

def cerrar_sesion(request):
    logout(request)
    return redirect('index')

def login_iniciar(request):
    username = request.POST.get('username','')
    password = request.POST.get('password','')
    print(username,password)
    user = authenticate(request,username=username,password=password)

    if user is not None:
        auth_login(request, user)
        return redirect("index")
    else:
        return redirect("index")

def registrarProducto(request):
    return render(request,'registrarProducto.html',{})

def crearProducto(request):
    nombreP = request.POST.get('nombreP','')
    costoPre = request.POST.get('costoPre','')
    costoReal = request.POST.get('costoReal','')
    tienda = request.POST.get('tienda','')
    notas = request.POST.get('notas','')
    imagen = request.FILES.get('imagen',False)
    producto = Producto(nombreP=nombreP,costoPre=costoPre,costoReal=costoReal,tienda=tienda,notas=notas,imagen=imagen)
    producto.save()
    return redirect('index')

def registrarTienda(request):
    return render(request,'registrarTienda.html',{})

def crearTienda(request):
    nombreT = request.POST.get('nombreT','')
    nombreS = request.POST.get('nombreS','')
    region = request.POST.get('region','')
    ciudad = request.POST.get('ciudad','')
    direccion = request.POST.get('direccion','')
    tienda = Tienda(nombreT=nombreT,nombreS=nombreS,region=region,ciudad=ciudad,direccion=direccion)
    tienda.save()
    return redirect('index')

class PersonaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Persona.objects.all().order_by('nombre')
    serializer_class = PersonaSerializer

class TiendaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Tienda.objects.all().order_by()
    serializer_class = TiendaSerializer

class ProductoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Producto.objects.all().order_by()
    serializer_class = ProductoSerializer