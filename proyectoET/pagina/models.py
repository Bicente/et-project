from django.db import models

# Create your models here.

class Producto(models.Model):
    nombreP = models.CharField(max_length=100)
    costoPre = models.IntegerField()
    costoReal = models.IntegerField()
    tienda = models.CharField(max_length=100)
    notas = models.CharField(max_length=500)
    imagen = models.ImageField(upload_to= 'media/')

    def __str__(self):
        return "producto" 

class Persona(models.Model):
    username = models.CharField(max_length=100)
    password  = models.CharField(max_length=100)
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    rut = models.CharField(max_length=15)
    fechNacimiento = models.DateField()
    correo = models.CharField(max_length=100)
    region = models.CharField(max_length=100)
    comuna = models.CharField(max_length=100)

    def __str__(self):
        return "persona" 

class Tienda(models.Model):
    nombreT = models.CharField(max_length=100)
    nombreS = models.CharField(max_length=100)
    region = models.CharField(max_length=100)
    ciudad = models.CharField(max_length=100)
    direccion = models.CharField(max_length=300)

    def __str__(self):
        return "tienda" 