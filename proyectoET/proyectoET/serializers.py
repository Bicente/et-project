from pagina.models import Persona, Tienda, Producto
from rest_framework import serializers

class PersonaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Persona
        fields = ('username','password','nombre','apellido','rut','fechNacimiento','correo','region','comuna')


class TiendaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tienda
        fields = ('nombreT','nombreS','region','ciudad','direccion')


class ProductoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Producto
        fields = ('nombreP','costoPre','costoReal','tienda','notas','imagen')