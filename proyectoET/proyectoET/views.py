from django.contrib.auth.models import Persona, Tienda, Producto
from rest_framework import serializers, viewsets
from proyectoET.serializers import PersonaSerializer, TiendaSerializer, ProductoSerializer

class PersonaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Persona.objects.all().order_by('nombre')
    serializer_class = PersonaSerializer

class TiendaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Tienda.objects.all().order_by()
    serializer_class = TiendaSerializer

class ProductoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Producto.objects.all().order_by()
    serializer_class = ProductoSerializer